package aulas.senai.indmo.exemplotabbed.ui.main;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import aulas.senai.indmo.exemplotabbed.PrimeiroFragment;
import aulas.senai.indmo.exemplotabbed.R;
import aulas.senai.indmo.exemplotabbed.SegundoFragment;
import aulas.senai.indmo.exemplotabbed.TerceiroFragment;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2, R.string.tab_text_3};
    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        //Criar um objeto da classe Fragment para retornar ao final
        Fragment fragment = null;
        //Usar um switch/case para verificar qual a posição da aba clicada
        switch (position){
            case 0: //Caso clicou na primeira aba
                //Chamamos a classe do PrimeiroFragment
                fragment = new PrimeiroFragment();
                break;
            case 1:
                fragment = new SegundoFragment();
                break;
            case 2:
                fragment = new TerceiroFragment();
                break;
        }
        return fragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return TAB_TITLES.length;
    }
}
